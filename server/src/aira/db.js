'use strict';

var PGP = require('pg-promise')();
var DBConfig = require("../../conf/database.config.json");
var Promise = require("promise");
var log = require("./log.js");
var Path = require("path");
var Env = process.env;
var readFile = Promise.denodeify(require('fs').readFile);
var caller = require("caller");

var lConnectionConfig = {
	"user": Env.AIRA_DB_USER || DBConfig.user,
	"password": Env.AIRA_DB_PASS, // no password store in config!
	"database": Env.AIRA_DB_NAME || DBConfig.dbname,
	"host": Env.AIRA_DB_HOST || DBConfig.host,
	"port": Env.AIRA_DB_PORT || DBConfig.port
};

var lDatabaseConnection = PGP(lConnectionConfig);

function any (aQuery) {
	log.debug(`Executing query to database and expecting anything: \n\t ${aQuery}`);
	return lDatabaseConnection.any(aQuery).catch(e => { log.error(e); throw null });
}

function many (aQuery) {
	log.debug(`Executing query to database and expecting one or more rows: \n\t ${aQuery}`);
	return lDatabaseConnection.many(aQuery).catch(e => { log.error(e); throw null });
}

function one (aQuery) {
	log.debug(`Executing query to database and expecting one row: \n\t ${aQuery}`);
	return lDatabaseConnection.one(aQuery).catch(e => { log.error(e); throw null });
}

function none (aQuery) {
	log.debug(`Executing query to database and expecting no rows: \n\t ${aQuery}`);
	return lDatabaseConnection.none(aQuery).catch(e => { log.error(e); throw null });
}

function oneOrNone (aQuery) {
	log.debug(`Executing query to database and expecting 0 or 1 rows: \n\t ${aQuery}`);
	return lDatabaseConnection.oneOrNone(aQuery).catch(e => { log.error(e); throw null });
}

function prepare(aQuery, aParams) {
	return Promise.resolve(PGP.as.format(aQuery, aParams || {}));
}

prepare.sync = function (aQuery, aParams) {
	return PGP.as.format(aQuery, aParams || {});
}


let db = { one, any, none, many, oneOrNone, execute: any, prepare };

module.exports = db;
