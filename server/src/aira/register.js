'use strict';

var Path = require('path');
var Log = require('./log.js');
var Utils = require('./utils.js')
var Responses = require('./responses.js');
var ServerConfig = require('../../conf/server.config.json');

function register(aModule, aMethod) {
	var lOID = Path.basename(aModule.filename, '.js');
	var lMethodId = makeMethodId(lOID);
	var lExportName = ServerConfig.methods[lMethodId]; 
	
	if (!lExportName) {
		throw new Error(`Unsupported access method id: ${lMethodId}`);
	}
	
	aModule.exports[lExportName] = (aRequest, aResponse) => {
		var lClientIP = aRequest.ips[0] || aRequest.ip;
		var lAcccesMethod = aRequest.method.toUpperCase();
		var lPath = aRequest.path;

		let error = (message) => {
			let lContent = Responses.error(message || `Service error. Error ticket is: ${Log.ticket}`);
			Log.debug(`Responding with: ${JSON.stringify(lContent)}`);
			aResponse.json(lContent);
		}
		let success = (message) => {
			let lContent = message || Responses.ok();
			Log.debug(`Responding with: ${JSON.stringify(lContent)}`);
			aResponse.json(lContent);
		}
		let lParams =  [];
		let lSwaggerParams = aRequest.swagger.params;
		Object.keys(lSwaggerParams).forEach(key => {
			lParams[key] = lSwaggerParams[key].value;
		});

		Log.context(aModule);
		Log.notice(`${lAcccesMethod}\t\t${lPath} from ${lClientIP}`);
		return aMethod(lParams, {error, success});
	};
}

function makeMethodId(anOID) {
	return anOID.split('').pop();
}

module.exports = register;
