'use strict';

var Path = require('path');
var Log = require('log');
var FS = require('fs');
var ConfDir = Path.resolve(__dirname, '..', '..', 'conf');
var LoggingConfig = require(Path.resolve(ConfDir, "logging.config.json"));
var MKDirP = require("mkdirp");
var Promise = require("promise");
var caller = require("caller");
var uid = require('./utils.js').uid;

var lLoggingDir = Path.resolve(ConfDir, LoggingConfig.dir);
var _loggersPool = {};
let context = { module: undefined, ticket: undefined };

//promesification...
var mkdirp = Promise.denodeify(MKDirP);
//...promesification

function setContext(aModule) {
	context.module = Path.basename(aModule.filename, '.js');
}

function lastErrorTicket() {
	return `${context.module}::${context.ticket}`;
}

function log(aLevel, aMessage) {
	var lModuleId = context.module;
	_getLogger(lModuleId)
		.then((aLogger) => {
			aLogger[aLevel](aMessage);
		})
		.catch((aError) => {
			console.error("Unable to log. Reason:\n\n%s", aError);
		});
}

function _initLogger(lModuleId) {
	return new Promise((resolve, reject) => {
		mkdirp(lLoggingDir)
			.then(() => {
				_loggersPool[lModuleId] = new Log(
					LoggingConfig.level,
					FS.createWriteStream(Path.join(lLoggingDir, lModuleId + ".log"), { 'flags': 'a' })
				);
				resolve(_loggersPool[lModuleId]);
			})
			.catch((aError) => {
				reject(aError);
			});
	});
}

function _getLogger(lModuleId) {
	return new Promise((resolve, reject) => {
		if (lModuleId in _loggersPool) {
			resolve(_loggersPool[lModuleId]);
		} else {
			_initLogger(lModuleId)
				.then((aLogger) => {
					resolve(aLogger);
				})
				.catch((aError) => {
					reject(aError);
				});
		}
	});
}

module.exports = {
	"emergency": log.bind(null, 'emergency'),
	"alert": log.bind(null, 'alert'),
	"critical": log.bind(null, 'critical'),
	"error": message => { context.ticket = uid(); log('error', `TICKETID: ${context.ticket}\n${message}`) },
	"warning": log.bind(null, 'warning'),
	"notice": log.bind(null, 'notice'),
	"info": log.bind(null, 'info'),
	"debug": log.bind(null, 'debug'),

	"context": setContext,
	"ticket": lastErrorTicket
}
