'use strict';

var arrayunique = require("array-unique");

let lTicketNumber = 1;
let uid = () => lTicketNumber++;

module.exports = { arrayunique, uid }