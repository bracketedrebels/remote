'use strict';

function makeError(aMessage) {
	return {
	  "message": aMessage
	};
}

function makeOk() {
	return {
		"status": "ok"
	};
}

module.exports = {
	"error": makeError,
	"ok": makeOk
}
