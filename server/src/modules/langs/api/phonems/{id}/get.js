'use strict';

let aira = require("aira.js");
let query = require("!text")("get.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { id: params.id })
	.then( aira.db.oneOrNone )
	.then( preapareSuccessHandler(params, respond) )
	.catch( respond.error );
});

function preapareSuccessHandler(params, respond) {
	return aResult => {
		if (aResult) {
			respond.success({
				"transcription": aResult.phonem,
				"id": Number(aResult.id),
				"tags": aResult.tags
			});
		}
		else {
			let lContent = `Phonem with id ${params.id} was not found`;
			aira.log.notice(lContent);
			respond.error(lContent);
		}
	}
}