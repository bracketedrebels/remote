'use strict';

var aira = require("aira.js");
let query = require("!text")("get.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { lang: params.lang, tags: params.tags || [] } )
	.then( aira.db.any )
	.then( prepareSuccessHandler(params, respond) )
	.catch( respond.error );
});

function prepareSuccessHandler(params, respond) {
	return aResult => {
		if (aResult instanceof Array ) {
			var lJsonResponse = {"rules": []};
			for (var row of aResult) {
				lJsonResponse.rules.push({
					"id": Number(row.id),
					"syntax": row.rule
				});
			}
			respond.success(lJsonResponse);
		}
		else {
			aira.log.error(`Invalid DB response: ${aResult}`);
			respond.error();
		}
	}
}