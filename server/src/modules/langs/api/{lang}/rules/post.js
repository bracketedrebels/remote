'use strict';

let aira = require("aira.js");
let query = require("!text")("post.sqls");

aira.register(module, (params, respond) => {
	let lQueryParams = {
		rule: params.rule,
		tags: aira.utils.arrayunique( params.tags || [] ),
		lang: params.lang
	};
	
	aira.db
	.prepare( query, lQueryParams )
	.then( aira.db.none )
	.then( respond.success )
	.catch( respond.error );
});