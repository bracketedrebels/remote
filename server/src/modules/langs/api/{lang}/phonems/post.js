'use strict';

let aira = require("aira.js");
let query = require("!text")("post.sqls");

aira.register(module, (params, respond) => {
	let lPhonem = params.transcription;
	let lTags = aira.utils.arrayunique(params.tags || []);
	let lLang = params.lang;
	
	aira.db
	.prepare( query, { phonem: lPhonem, lang: lLang, tags: lTags } )
	.then( aira.db.none )
	.then( respond.error )
	.catch( respond.error );
});
