'use strict';

var aira = require("aira.js");
let query = require("!text")("get.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { lang: params.lang, tags: params.tags || [] } )
	.then( aira.db.any )
	.then( prepareSucceedHandler(params, respond) )
	.catch( respond.error );
});

function prepareSucceedHandler(params, respond) {
	return aResult => {
		if (aResult instanceof Array) {
			let lPhonemsList = [];
			let lJsonResponse = {"phonems": lPhonemsList};
			aResult.forEach(row => lPhonemsList.push({
				"id": Number(row.id), "transcription": row.phonem
			}));
			respond.success(lJsonResponse);
		}
		else {
			aira.log.error(`Invalid DB response: ${aResult}`);
			respond.error();
		}
	}
}