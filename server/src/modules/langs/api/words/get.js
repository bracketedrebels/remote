'use strict';

let aira = require("aira.js");
let RandExp = require("randexp").randexp;
let mainQuery = require("!text")("get.mainquery.sqls");
let subQuery = require("!text")("get.subquery.sqls");

aira.register(module, (params, respond) => {
	var lQuery = prepareQuery(params);
	
	aira.db
	.many(lQuery)
	.then( aResult => respond.success(generateSamples(params.amount, aResult, params.rule)) )
	.catch( respond.error );
});

function prepareQuery(params) {
	var lQueries = [];
	var lPhonemSelectors = parseRule(params.rule);
	for (var lSelectorId in lPhonemSelectors) {
		var lTagsList = parsePhonemSelector(lPhonemSelectors[lSelectorId]);
		lQueries.push(aira.db.prepare.sync(subQuery, {
			selectorid: lSelectorId,
			tags: lTagsList || []
		}	
		));
	}
	var lSubQuery = `(${lQueries.join( ') UNION (' )})`;
	var lFinalQuery = aira.db.prepare.sync(mainQuery, {subquery: lSubQuery});
	return lFinalQuery;
}

function parseRule(aRule) {
	return aRule.match(/<(?:\w+(?:-\w+)*)?>/ig);
}

function parsePhonemSelector(aSelector) {
	var lTrimmedSelector = aSelector.slice(1, aSelector.length - 1);
	var lTagsList = lTrimmedSelector.split('-');
	lTagsList = lTagsList.filter((aItem) => Boolean(aItem));
	return lTagsList.length > 0 ? aira.utils.arrayunique(lTagsList) : lTagsList;
}

function generateSamples (aSamplesAmount, aSelectorsIdsToPhonemsMap, aRule) {
	var lRule = aRule;
	var lSamples = [];
	var lPhonemSelectors = parseRule(aRule);
	for (var lMap of aSelectorsIdsToPhonemsMap) {
		var lPhonemsList = lMap.phonems;
		var lSelector = lPhonemSelectors[lMap.selectorid];
		var lSerializedPhonemsList = '(' + lPhonemsList.join('|') + ')';
		lRule = lRule.replace(lSelector, lSerializedPhonemsList);
	}
	while (aSamplesAmount--) {
		lSamples.push(RandExp(lRule));
	}
	return lSamples;
}
