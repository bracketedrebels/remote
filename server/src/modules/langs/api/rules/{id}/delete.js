'use strict';

let aira = require("aira.js");
let query = require("!text")("delete.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { id: params.id })
	.then( aira.db.none )
	.then( respond.success )
	.catch( respond.error );
});