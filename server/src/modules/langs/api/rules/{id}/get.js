'use strict';

let aira = require("aira.js");
let query = require("!text")("get.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { id: params.id } )
	.then( aira.db.oneOrNone )
	.then( prepareSuccessHandler(params, respond) )
	.catch( respond.error );
});

function prepareSuccessHandler(params, respond) {
	return aResult => {
		if (aResult) {
			respond.success({
				"id": Number(aResult.id),
				"syntax": aResult.rule,
				"tags": aResult.tags
			});
		}
		else {
			let lContent = `Rule with id ${params.id} was not found`;
			aira.log.notice(lContent);
			respond.error(lContent);
		}
	}
}