'use strict';

var aira = require("aira.js");
let query = require("!text")("post.sqls");

aira.register(module, (params, respond) => {
	aira.db
	.prepare( query, { title: params.title } )
	.then( aira.db.none )
	.then( respond.success )
	.catch( respond.error );
});
