'use strict';

let aira = require("aira");
let query = require("!text")("get.sqls");

aira.register(module, (params, respond) => {
	aira.db.any( query )
	.then( prepareSuccessResponseHandler(params, respond) )
	.catch( respond.error );
});

function prepareSuccessResponseHandler(params, respond) {
	return aResult => {
		if (aResult instanceof Array) {
			respond.success(aResult.map(value => value.name));
		}
		else {
			aira.log.error(`Invalid DB response: ${aResult}`);
			respond.error();
		}
	}
}