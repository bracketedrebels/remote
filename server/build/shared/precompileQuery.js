'use strict';

let details = require("../shared/details.js");
let Pathes = require("../shared/pathes.js");
let DBConfig = require(Pathes.confs.db);
let PGP = require('pg-promise')();
let Path = require("path");

module.exports = (aQuery, anExecutorFile) => {
	let lParams = { schema_tagging: DBConfig.schemas.tagging };

	if (anExecutorFile) {
		let lLocalPathToFile = Path.relative(Pathes.dirs.modules, anExecutorFile);
		let lDetails = details(lLocalPathToFile);
		lParams.schema_api = lDetails.module_id;
	}


	return PGP.as.format(aQuery, lParams, { partial: true });
};
