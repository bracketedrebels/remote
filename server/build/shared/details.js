'use strict';

let Pathes = require("./pathes.js");
let ServerConfig = require(Pathes.confs.server);
let Hash = require("string-hash");
let Path = require("path");

module.exports = (aLocalModelPath) => {
	let lParsedPath = aLocalModelPath.split(Path.sep);
	let lMethod = lParsedPath.pop().split('.')[0];
	let lRESTModuleID = lParsedPath[0];
	let lRESTPath = '/' + lParsedPath.filter(value => value !== 'api').join('/');
	let lOID = "OID_"
		+ String(Hash(lRESTPath) + 1).match(/.{1,5}/g).join("_")
		+ String(ServerConfig.methods.indexOf(lMethod));

	return {
		"OID": lOID,
		"method": lMethod,
		"operationid": lMethod,
		"path": lRESTPath,
		"module_id": lRESTModuleID
	}
}