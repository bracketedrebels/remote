'use strict';

var Path = require("path");

var ConfPath = Path.resolve(__dirname, '..', '..', 'conf');

var BuildConfigPath = Path.resolve(ConfPath, "build.config.json");
var ServerConfigPath = Path.resolve(ConfPath, "server.config.json");
var DatabaseConfigPath = Path.resolve(ConfPath, "database.config.json");
var LoggingConfigPath = Path.resolve(ConfPath, "logging.config.json");

var BuildConfig = require(BuildConfigPath);

var lApiDir = Path.resolve(ConfPath, BuildConfig.to);
var lSrcDir = Path.resolve(ConfPath, BuildConfig.src);
var lConfigDir = Path.resolve(lApiDir, BuildConfig.conf);
var lDBInitScriptsDir = Path.resolve(ConfPath, BuildConfig.init);
var lAIRADir = Path.resolve(lApiDir, 'node_modules');
var lModulesDir = Path.resolve(lSrcDir, 'modules');
var lAPIModulesDir = Path.resolve(lApiDir, 'modules');
var lSchemasDir = Path.resolve(lSrcDir, 'schemas');
var lApiSwaggerFile = Path.resolve(lApiDir, 'swagger.yaml');

module.exports = {
	dirs: {
		api: lApiDir,
		src: lSrcDir,
		conf: lConfigDir,
		confs: ConfPath,
		dbscripts: lDBInitScriptsDir,
		aira: lAIRADir,
		modules: lModulesDir,
		apimodules: lAPIModulesDir, 
		schemas: lSchemasDir
	},
	confs: {
		build: BuildConfigPath,
		server: ServerConfigPath,
		db: DatabaseConfigPath,
		log: LoggingConfigPath
	},
	swagger: lApiSwaggerFile
}