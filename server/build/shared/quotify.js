module.exports = function (aTarget) {
	return `"${aTarget.replace(/([^\\])"/g, '$1\\"')}"`;
}