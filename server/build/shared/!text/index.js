'use strict';

let Path = require('path');
let FS = require('fs');
let Caller = require('caller');

module.exports = filename => String(FS.readFileSync(Path.resolve(Path.dirname(Caller()), filename)));