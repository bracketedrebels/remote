'use strict';

let Pathes = require("./pathes.js");
let ServerConfig = require(Pathes.confs.server);
var Package = require("../../package.json");

var lDefinitions = {};
var lPaths = {};
lPaths[ServerConfig.prefix] = { "x-swagger-pipe": "swagger_raw" };

module.exports = {
	"swagger": "2.0",
	"info": {
		"version": Package.version,
		"title": ServerConfig.name
	},
	"host": [ServerConfig.host, ServerConfig.port].join(':'),
	"basePath": ServerConfig.prefix,
	"schemes": [
		ServerConfig.https ? "https" : "http"],
	"produces": [
		"application/json"],
	"securityDefinitions": {
		"apikey": {
			"type": "apiKey",
			"name": "X-API-KEY",
			"in": "header"
		}
	},
	"security": [{
		"apikey": []
	}],
	"paths": lPaths,
	"definitions": lDefinitions
};