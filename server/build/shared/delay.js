'use strict';

module.exports = function (aCallback) {
	return function () {
		let lBindingArgs = [null].concat(Array.prototype.slice.call(arguments));
		return Function.prototype.bind.apply(aCallback, lBindingArgs);
	}
}