'use strict';

let details = require("../shared/details.js");
let precompileQuery = require("../shared/precompileQuery.js");
let Pathes = require("../shared/pathes.js");
let quotify = require("../shared/quotify.js");
let Find = require('globby');
let Promise = require("promise");
let ServerConfig = require(Pathes.confs.server);
let DBConfig = require(Pathes.confs.db);
let Path = require("path");
let StaticModule = require('static-module');
let Quote = require('quote-stream');
let FSE = require('fs-extra');

let lTargetControllersDir = Path.resolve(Pathes.dirs.api, 'controllers');

module.exports = () => {
	return Find(['**/*{' + ServerConfig.methods.join(',') + '}*.js'], {
		cwd: Pathes.dirs.modules,
		nodir: true,
		realpath: false
	})
	.then(aFilesList => new Promise((resolve, reject) => {
		FSE.mkdirsSync(lTargetControllersDir);
		return recursiveControllerMigration(aFilesList, resolve, reject);
	}));
}

function recursiveControllerMigration(aFilesList, resolve, reject) {
	var lFileName = aFilesList.pop();

	if (lFileName) {
		let lDetails = details(lFileName);

		let lSourceControllerFile = Path.resolve(Pathes.dirs.modules, lFileName);
		let lTargetControllerFile = Path.resolve(lTargetControllersDir, lDetails.OID + '.js');
		let lInStream = FSE.createReadStream(lSourceControllerFile);
		let lOutStream = FSE.createWriteStream(lTargetControllerFile);
		let lBundler = StaticModule({
			"!text": file => {
				let lAssumedToBeQuery = String(FSE.readFileSync(
					Path.join(Path.dirname(lSourceControllerFile), file)
				));
				return quotify(precompileQuery(lAssumedToBeQuery, lSourceControllerFile));	
			}
		},{ 
			vars: { "!text": require('!text')}
		});

		lOutStream.on('finish', () => {
			return recursiveControllerMigration(aFilesList, resolve, reject);
		});
		lOutStream.on('error', reject);
		lInStream.on('error', reject);

		lInStream
		.pipe(lBundler)
		.pipe(lOutStream);
	}
	else {
		resolve();
	}
}