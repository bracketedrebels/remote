'use strict';

let Find = require('globby');
let Promise = require("promise");
let Path = require("path");
let FS = require('fs');

let Pathes = require("../shared/pathes.js");
let delay = require("../shared/delay.js");
let precompileQuery = require("../shared/precompileQuery.js");

module.exports = (aDatabaseConnection) => {
	return Find(['*/manifest.json'], {
		cwd: Pathes.dirs.modules,
		nodir: true,
		realpath: true
	})
	.then((aFilesList) => new Promise((resolve, reject) =>
		recursiveModuleInstallation(aDatabaseConnection, aFilesList, resolve, reject)
	));
}

function recursiveModuleInstallation(aDatabaseConnection, aFilesList, resolve, reject) {
	let lManifestPath = aFilesList.shift();

	if (lManifestPath) {
		let lSQLInitPath = Path.join(Path.dirname(lManifestPath), "init.sqls");
		let lParametrizedQuery = String(FS.readFileSync(lSQLInitPath)); 
		let lPrecompiledQuery = precompileQuery(lParametrizedQuery, lSQLInitPath);
		
		aDatabaseConnection.any( lPrecompiledQuery )
		.catch( reject )
		.then( () => recursiveModuleInstallation(aDatabaseConnection, aFilesList, resolve, reject) );
	}
	else {
		resolve( );
	}
}
