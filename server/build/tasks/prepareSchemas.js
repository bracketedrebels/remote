'use strict';

let Find = require('globby');
let Pathes = require('../shared/pathes.js');
let SwaggerObject = require('../shared/swagger.js');
let Path = require('path');

module.exports = () => {
	return Find(['*.yaml'], {
		cwd: Pathes.dirs.schemas,
		nodir: true,
		realpath: false
	})
	.then( aFilesList => {
		for (let lFileName of aFilesList) {
			let lDefinitionName = Path.basename(lFileName, ".yaml");
			SwaggerObject.definitions[lDefinitionName] = { "$ref": Path.resolve(Pathes.dirs.schemas, lFileName) };
		}
	});
}