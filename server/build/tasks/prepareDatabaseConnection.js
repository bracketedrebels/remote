'use strict';

let Path = require("path");
let Pathes = require("../shared/pathes.js");
let DatabaseConfig = require(Pathes.confs.db);
let Promise = require("promise");
let read = Promise.denodeify(require('read'));
let PGP = require('pg-promise')();

module.exports = () => {
	return read({
		prompt: "Enter '" + DatabaseConfig.user + "' user password: ",
		silent: true,
		replace: "*"
	})
	.then((aPassword) => new Promise((resolve, reject) => {
		resolve(PGP({
			"database" : DatabaseConfig.dbname,
			"user" : DatabaseConfig.user,
			"host" : DatabaseConfig.host,
			"port" : DatabaseConfig.port,
			"password" : aPassword
		}));
	}));
}