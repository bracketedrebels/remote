'use strict';

let Path = require("path");
let Pathes = require("../shared/pathes.js");
let Promise = require("promise");
let readfile = Promise.denodeify(require("fs").readFile);
let Find = require('globby');
let precompileQuery = require('../shared/precompileQuery.js');

module.exports = aDatabaseConnection => {

	return Find(['*.sqls'], {
		cwd: Pathes.dirs.dbscripts,
		nodir: true,
		realpath: true
	})
	.then((aFilesList) => new Promise((resolve, reject) => {
			recursiveStatementsFilesExecution(aDatabaseConnection, aFilesList, resolve, reject)
		}
	));
}

function recursiveStatementsFilesExecution(aDatabaseConnection, aFilesList, resolve, reject) {
	let lExecutionFile = aFilesList.shift();

	if (lExecutionFile) {
		readfile( lExecutionFile )
		.then( aStatementsString => precompileQuery(aStatementsString.toString()) )
		.then( aQuery => aDatabaseConnection.any(aQuery) )
		.then( () => recursiveStatementsFilesExecution(aDatabaseConnection, aFilesList, resolve, reject) )
		.catch( reject );
	}
	else {
		resolve( aDatabaseConnection );
	}
}