'use strict';

let Path = require("path");
let Promise = require("promise");
let FS = require("fs-promise");
let Pathes = require("../shared/pathes.js");
let delay = require("../shared/delay.js");

//tasks...
let bundleSwagger = require("./bundleSwagger.js");
let migrateControllers = require("./migrateControllers.js");
let prepareModulesModels = require("./prepareModulesModels.js");
let prepareSchemas = require("./prepareSchemas.js");
let prepareRequirements = require("./prepareRequirements.js");
//...tasks

//promesification...
let mkdirp = delay(require('mkdirp-promise'));
let copy = delay(FS.copy);
//...promesification

//vars...
let lApiDir = Pathes.dirs.api;
let lAIRADir = Pathes.dirs.aira;
let lConfigDir = Pathes.dirs.conf;
let lSrcDir = Pathes.dirs.src;
let lConfPath = Pathes.dirs.confs;
//...vars

//building...
module.exports = () =>  mkdirp( lApiDir, parseInt('0777', '8') )()
	.then( mkdirp(lAIRADir, parseInt('0777', '8')) )
	.then( mkdirp(lConfigDir, parseInt('0777', '8')) )
	.then( copy(Path.join(lSrcDir, 'aira'), Path.join(lAIRADir, 'aira')) )
	.then( copy(Path.join(lSrcDir, 'aira.js'), Path.join(lAIRADir, 'aira.js')) )
	.then( copy(Path.join(lSrcDir,'default.yaml'), Path.join(lConfigDir,'default.yaml')) )
	.then( copy(lConfPath, Path.join(lApiDir, 'conf')) )
	.then( prepareRequirements )
	.then( prepareModulesModels )
	.then( prepareSchemas )
	.then( bundleSwagger )
	.then( migrateControllers )
	.then( () => console.log("Build updated") )
	.catch( e => console.error(e) );