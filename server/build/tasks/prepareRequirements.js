'use strict';

let FSE = require('fs-extra');
let Promise = require('promise');
let Path = require('path');

module.exports = () => new Promise((resolve, reject) => {
	FSE.copy(
		Path.resolve(__dirname, '..', 'shared', '!text'),
		Path.resolve(__dirname, '..', '..', 'node_modules', '!text'),
		{ clobber: true },
		err => err ? reject(err) : resolve()
	);
});