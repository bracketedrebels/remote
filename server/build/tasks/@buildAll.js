'use strict';

let buildCode = require('./@buildCode.js');
let buildDatabase = require('./@buildDatabase.js');

module.exports = () => 
	buildCode()
	.then( buildDatabase )
	.then( () => console.log('Done') )
	.catch( e => console.error(e) );