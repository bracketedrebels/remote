var SwaggerParser = require("swagger-parser");
var JSON2YAML = require("json2yaml");
var SwaggerObject = require("../shared/swagger.js");
var Promise = require("promise");
var writefile = Promise.denodeify(require('fs').writeFile);
var Pathes = require('../shared/pathes.js');

module.exports = () => 
	SwaggerParser.validate(SwaggerObject, {
		"$refs": { 
			"internal": false,
			"external": true
		}
	})
	.then( api => writefile(Pathes.swagger, JSON2YAML.stringify(api)) );