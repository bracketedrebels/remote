'use strict';

//tasks...
let installDatabase = require("./installDatabase.js");
let installModules = require("./installModules.js");
let prepareDatabaseConnection = require("./prepareDatabaseConnection.js");
//...tasks

module.exports = () => prepareDatabaseConnection()
.then( installDatabase )
.then( installModules )
.then( () => console.log('Database updated') )
.catch( e => console.error(e) );