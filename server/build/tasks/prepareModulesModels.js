'use strict';

let Path = require('path');
let Find = require('globby');
let Pathes = require('../shared/pathes.js');
let ServerConfig = require(Pathes.confs.server);

let SharedSwaggerObject = require("../shared/swagger.js");
let details = require("../shared/details.js");

module.exports = () => {
	return Find(['**/*{' + ServerConfig.methods.join(',') + '}*.yaml'], {
		cwd: Pathes.dirs.modules,
		nodir: true,
		realpath: false
	})
	.then((aFilesList) => {
		aFilesList.forEach(prepareModuleModel);
	});
}

function prepareModuleModel(aModelFile) {
	var lDetails = details(aModelFile);
	var lModelFile = Path.resolve(Pathes.dirs.modules, aModelFile);
	var lModel = SharedSwaggerObject.paths;

	lModel[lDetails.path] = lModel[lDetails.path] || {};
	lModel[lDetails.path][lDetails.method] = {
		"x-swagger-router-controller": lDetails.OID,
		"operationId": lDetails.operationid,
		"$ref": lModelFile
	};
}