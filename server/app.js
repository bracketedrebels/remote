'use strict';

var Path = require('path');
var FS = require("fs");
var Promise = require("promise");
var Server = require('express')();
var SwaggerExpress = require('swagger-express-mw');
var LetsencryptExpress = require('letsencrypt-express');
var Package = require('./package.json');
var ConfDir = Path.resolve(__dirname, 'conf');
var BuildConfig = require(Path.resolve(ConfDir, 'build.config.json'));
var ServerConfig = require(Path.resolve(ConfDir, 'server.config.json'));

var lTargetBuildDir = Path.resolve(ConfDir, BuildConfig.to);
var lTargetConfigDir = Path.resolve(lTargetBuildDir, 'config');
var lTargetSwaggerFile = Path.resolve(lTargetBuildDir, 'swagger.yaml');
var AIRA = require(Path.resolve(lTargetBuildDir, 'node_modules', 'aira.js'));

//promesification...
var swaggerCreate = Promise.denodeify(SwaggerExpress.create);
//...promesification

swaggerCreate({
	appRoot: lTargetBuildDir,
  configDir: lTargetConfigDir,
  swaggerFile: lTargetSwaggerFile
})
.then(swaggerRegister)
.then(startLEX)
.catch(AIRA.log.error);

function onServerReady() {
	var server = this;
	var protocol = ('requestCert' in server) ? 'https' : 'http';
	console.log("Listening at " + protocol + '://localhost:' + this.address().port + "\n");
}

function swaggerRegister(aSwaggerExpress) {
  return new Promise((resolve, reject) => {
    try {
      aSwaggerExpress.register(Server);
      resolve();
    }
    catch (e) {
      reject(e);
    }
  })
}

function startLEX () {
  var lPort_num = ServerConfig.port || 33003;
  var lHTTPSPorts_num_arr = ServerConfig.https ? [lPort_num] : [];
  var lHTTPPorts_num_arr = ServerConfig.https ? [] : [lPort_num];
  var lLEX = LetsencryptExpress.create({
    debug: true
  });
  lLEX.onRequest = Server;
  lLEX.listen(lHTTPPorts_num_arr, lHTTPSPorts_num_arr, onServerReady);
}
